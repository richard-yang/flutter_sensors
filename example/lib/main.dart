import 'dart:async';
import 'dart:math' as math;
import 'package:flutter/material.dart';
import 'package:flutter_sensors/flutter_sensors.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool _accelAvailable = false;
  bool _gyroAvailable = false;
  bool _rotAvailable = false;
  List<double> _accelData = List.filled(3, 0.0);
  List<double> _gyroData = List.filled(3, 0.0);
  List<double> _rotData = List.filled(3, 0.0);
  StreamSubscription? _accelSubscription;
  StreamSubscription? _gyroSubscription;
  StreamSubscription? _rotSubscription;

  @override
  void initState() {
    _checkAccelerometerStatus();
    _checkGyroscopeStatus();
    _checkRotationStatus();
    super.initState();
  }

  @override
  void dispose() {
    _stopAccelerometer();
    _stopGyroscope();
    _stopRotation();
    super.dispose();
  }

  void _checkAccelerometerStatus() async {
    await SensorManager()
        .isSensorAvailable(Sensors.ACCELEROMETER)
        .then((result) {
      setState(() {
        _accelAvailable = result;
      });
    });
  }

  Future<void> _startAccelerometer() async {
    if (_accelSubscription != null) return;
    if (_accelAvailable) {
      final stream = await SensorManager().sensorUpdates(
        sensorId: Sensors.ACCELEROMETER,
        interval: Sensors.SENSOR_DELAY_FASTEST,
      );
      _accelSubscription = stream.listen((sensorEvent) {
        setState(() {
          _accelData = sensorEvent.data;
        });
      });
    }
  }

  void _stopAccelerometer() {
    if (_accelSubscription == null) return;
    _accelSubscription?.cancel();
    _accelSubscription = null;
  }

  void _checkGyroscopeStatus() async {
    await SensorManager().isSensorAvailable(Sensors.GYROSCOPE).then((result) {
      setState(() {
        _gyroAvailable = result;
      });
    });
  }

  Future<void> _startGyroscope() async {
    if (_gyroSubscription != null) return;
    if (_gyroAvailable) {
      final stream =
          await SensorManager().sensorUpdates(sensorId: Sensors.GYROSCOPE);
      _gyroSubscription = stream.listen((sensorEvent) {
        setState(() {
          _gyroData = sensorEvent.data;
        });
      });
    }
  }

  void _stopGyroscope() {
    if (_gyroSubscription == null) return;
    _gyroSubscription?.cancel();
    _gyroSubscription = null;
  }

  void _checkRotationStatus() async {
    await SensorManager()
        .isSensorAvailable(Sensors.GAME_ROTATION)
        .then((result) {
      setState(() {
        _rotAvailable = result;
      });
    });
  }

  Future<void> _startRotation() async {
    if (_rotSubscription != null) return;
    if (_rotAvailable) {
      final stream = await SensorManager().sensorUpdates(
        sensorId: Sensors.GAME_ROTATION,
        interval: Sensors.SENSOR_DELAY_UI,
      );
      _rotSubscription = stream.listen((sensorEvent) {
        setState(() {
          _rotData = sensorEvent.data;
          print('rotation: $_rotData');
        });
      });
    }
  }

  void _stopRotation() {
    if (_rotSubscription == null) return;
    _rotSubscription?.cancel();
    _rotSubscription = null;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Flutter Sensors Example'),
        ),
        body: Container(
          padding: EdgeInsets.all(16.0),
          alignment: AlignmentDirectional.topCenter,
          child: Column(
            children: <Widget>[
              Text(
                "Accelerometer Test",
                textAlign: TextAlign.center,
              ),
              Text(
                "Accelerometer Enabled: $_accelAvailable",
                textAlign: TextAlign.center,
              ),
              Padding(padding: EdgeInsets.only(top: 16.0)),
              Text(
                "[0](X) = ${_accelData[0]}",
                textAlign: TextAlign.center,
              ),
              Padding(padding: EdgeInsets.only(top: 16.0)),
              Text(
                "[1](Y) = ${_accelData[1]}",
                textAlign: TextAlign.center,
              ),
              Padding(padding: EdgeInsets.only(top: 16.0)),
              Text(
                "[2](Z) = ${_accelData[2]}",
                textAlign: TextAlign.center,
              ),
              Padding(padding: EdgeInsets.only(top: 16.0)),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  MaterialButton(
                    child: Text("Start"),
                    color: Colors.green,
                    onPressed:
                        _accelAvailable ? () => _startAccelerometer() : null,
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                  ),
                  MaterialButton(
                    child: Text("Stop"),
                    color: Colors.red,
                    onPressed:
                        _accelAvailable ? () => _stopAccelerometer() : null,
                  ),
                ],
              ),
              Padding(padding: EdgeInsets.only(top: 16.0)),
              Text(
                "Gyroscope Test",
                textAlign: TextAlign.center,
              ),
              Text(
                "Gyroscope Enabled: $_gyroAvailable",
                textAlign: TextAlign.center,
              ),
              Padding(padding: EdgeInsets.only(top: 16.0)),
              Text(
                "[0](X) = ${_gyroData[0]}",
                textAlign: TextAlign.center,
              ),
              Padding(padding: EdgeInsets.only(top: 16.0)),
              Text(
                "[1](Y) = ${_gyroData[1]}",
                textAlign: TextAlign.center,
              ),
              Padding(padding: EdgeInsets.only(top: 16.0)),
              Text(
                "[2](Z) = ${_gyroData[2]}",
                textAlign: TextAlign.center,
              ),
              Padding(padding: EdgeInsets.only(top: 16.0)),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  MaterialButton(
                    child: Text("Start"),
                    color: Colors.green,
                    onPressed: _gyroAvailable ? () => _startGyroscope() : null,
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                  ),
                  MaterialButton(
                    child: Text("Stop"),
                    color: Colors.red,
                    onPressed: _gyroAvailable ? () => _stopGyroscope() : null,
                  ),
                ],
              ),
              Padding(padding: EdgeInsets.only(top: 16.0)),
              Text(
                "Rotation Test",
                textAlign: TextAlign.center,
              ),
              Text(
                "Rotation Enabled: $_rotAvailable",
                textAlign: TextAlign.center,
              ),
              Padding(padding: EdgeInsets.only(top: 16.0)),
              Row(
                children: [
                  Expanded(
                    child: LinearProgressIndicator(
                      color: Colors.blue,
                      value: _rotData[0] / 2 + 0.5,
                    ),
                  ),
                  SizedBox(
                    width: 30,
                    child: Text('${(_rotData[0] * 100).toInt()}',
                        textAlign: TextAlign.right),
                  ),
                ],
              ),
              Padding(padding: EdgeInsets.only(top: 16.0)),
              Row(
                children: [
                  Expanded(
                    child: LinearProgressIndicator(
                      color: Colors.blue,
                      value: _rotData[1] / 2 + 0.5,
                    ),
                  ),
                  SizedBox(
                    width: 30,
                    child: Text('${(_rotData[1] * 100).toInt()}',
                        textAlign: TextAlign.right),
                  ),
                ],
              ),
              Padding(padding: EdgeInsets.only(top: 16.0)),
              Row(
                children: [
                  Expanded(
                    child: LinearProgressIndicator(
                      color: Colors.blue,
                      value: _rotData[2] / 2 + 0.5,
                    ),
                  ),
                  SizedBox(
                    width: 30,
                    child: Text('${(_rotData[2] * 100).toInt()}',
                        textAlign: TextAlign.right),
                  ),
                ],
              ),
              if (_rotData.length > 3)
                Padding(padding: EdgeInsets.only(top: 16.0)),
              if (_rotData.length > 3)
                Row(
                  children: [
                    Expanded(
                      child: LinearProgressIndicator(
                        color: Colors.blue,
                        value: _rotData[3] / 2 + 0.5,
                      ),
                    ),
                    SizedBox(
                      width: 30,
                      child: Text('${(_rotData[3] * 100).toInt()}',
                          textAlign: TextAlign.right),
                    ),
                  ],
                ),
              if (_rotData.length > 4)
                Padding(padding: EdgeInsets.only(top: 16.0)),
              if (_rotData.length > 4)
                Row(
                  children: [
                    Expanded(
                      child: LinearProgressIndicator(
                        color: Colors.blue,
                        value: _rotData[4],
                      ),
                    ),
                    SizedBox(
                      width: 30,
                      child: Text('${(_rotData[4] * 100).toInt()}',
                          textAlign: TextAlign.right),
                    ),
                  ],
                ),
              Padding(padding: EdgeInsets.only(top: 16.0)),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  MaterialButton(
                    child: Text("Start"),
                    color: Colors.green,
                    onPressed: _rotAvailable ? () => _startRotation() : null,
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                  ),
                  MaterialButton(
                    child: Text("Stop"),
                    color: Colors.red,
                    onPressed: _rotAvailable ? () => _stopRotation() : null,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
